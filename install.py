#!/usr/bin/env python3

import sys, getopt, os
import configparser
import shutil
from pathlib import Path
from getpass import getpass

def print_info(info: str):
    os.system("echo [$'\e[1;35m'INFO$'\e[0m'] " + info)

def print_success(success: str):
    os.system("echo [$'\e[1;32m'SUCCESS$'\e[0m'] " + success)

def print_fail(fail: str):
    os.system("echo [$'\e[1;31m'FAIL$'\e[0m']" + fail)
    sys.exit()

source_files = ['API.py', 'Spotifli.py', 'Data.py', 'UI.py', 'Threader.py']

def files():
    # overwrite all files
    if os.path.isdir(str(Path.home()) + "/.Spotifli") == False:
        os.mkdir(str(Path.home()) + "/.Spotifli")
    if os.path.isdir(str(Path.home()) + "/.Spotifli/src") == False:
        os.mkdir(str(Path.home()) + "/.Spotifli/src")

    for file in source_files:
        shutil.copy2(".Spotifli/src/" + file, str(Path.home()) + '/.Spotifli/src/' + file)

    if os.path.isdir(str(Path.home()) + "/.Spotifli/src/Templates") == False:
        os.mkdir(str(Path.home()) + "/.Spotifli/src/Templates")

    for fname in os.listdir('.Spotifli/src/Templates/'):
        shutil.copy2('.Spotifli/src/Templates/'+fname, str(Path.home()) + '/.Spotifli/src/Templates/'+fname)

    shutil.copy2("spotifli", str(Path.home()) + "/.local/bin/spotifli")
    
    print("Please put ~/.local/bin into your path variable!!!")

def help ():
    print('install.py <args>')
    print('''  -h
    help
  -a
    Launch will all options: install, copy files, configure
  -c
    Configure applications config files
  -i
    Install application dependencies
  -f
    Copy source files to .Spotifly/src and copy executable to ~/.local/bin
''')
    sys.exit()

def config():
    path = str(Path.home()) + "/.Spotifli/spotifli.conf"
    
    shutil.copy('.Spotifli/spotifli.conf', str(Path.home()) + '/.Spotifli/spotifli.conf')

    if os.path.isdir(str(Path.home()) + "/.config/spotifyd") == False:
        os.mkdir(str(Path.home()) + "/.config/spotifyd")
    shutil.copy('.Spotifli/spotifyd.conf', str(Path.home()) + '/.config/spotifyd/spotifyd.conf')
    
    if os.path.isdir(str(Path.home()) + "/.config/systemd") == False:
        os.mkdir(str(Path.home()) + "/.config/systemd/")
         
    if os.path.isdir(str(Path.home()) + "/.config/systemd/user") == False:
        os.mkdir(str(Path.home()) + "/.config/systemd/user")

    shutil.copy('.Spotifli/spotifyd.service', str(Path.home()) + '/.config/systemd/user/spotifyd.service')

    config = configparser.ConfigParser()
    config.read(path)
    os.system("echo -e \"Follow this link to create an app:\n\033[0;33mhttps://developer.spotify.com/documentation/general/guides/authorization/app-settings/\033[0m\n\"")

    os.system('echo "------------------------------------------------------------"')

    os.system('echo "Or follow these directions."')
    os.system('echo "1. Go to the spotify dashboard: "')
    os.system('echo -e "    - \033[0;33mhttps://developer.spotify.com/dashboard/login\033[0m"')
    os.system('echo "2. Login to your spotify premium account"')
    os.system('echo -e "3. Click on \033[0;35mCreate an App\033[0m"') # Make color on Create an App
    os.system('echo "4. Follow the instruction and prompts"')
    os.system('echo "5. It should automatically take you to the new app."')
    os.system('echo "    Otherwise select the app you just created."')
    os.system('echo "6. Click \033[0;35mEdit Settings\033[0m"') # color on Click Edit Settings
    os.system('echo "7. Enter a rediret URI | ex. http://localhost:\033[0;32m8888\033[0m/callback"')
    os.system('echo "    - Note: It must be of the format"')
    os.system('echo "    - http://localhost:{\033[0;32m4 digits\033[0m}/callback"')
    os.system('echo "8. Go back to app and click \033[0;35mShow Client Secret\033[0m"') # Color on Show Client Secret
    os.system('echo "9. Use the \033[0;32mport\033[0m from step 7 along with the "')
    os.system('echo "    \033[0;34mclient ID\033[0m and \033[0;31mclient secret\033[0m to fill in the information "')
    os.system("echo -e \"    below in the \033[1;36mConfig Setup\033[0m\"") # Color on config setup
    os.system('echo "------------------------------------------------------------\n"')

    os.system("echo $'\e[1;36m'Config Setup$'\e[0m'")

    print()
    os.system("echo -e \"1. Enter your \033[0;34mclient id\033[0m:\"")
    config['Login']['user_id'] = input()

    print()
    os.system("echo -e \"2. Enter your \033[0;31mclient secret\033[0m:\"")
    config['Login']['user_secret'] = input()

    print()
    os.system("echo -e \"3. Enter the local \033[0;32mport\033[0m for login redirect (ex. \033[0;32m8888\033[0m):\"")
    config['Login']['port'] = input('')

    with open(path, "+w") as config_file:
        config.write(config_file)

    path = str(Path.home()) + "/.config/spotifyd/spotifyd.conf"
    
    config = configparser.ConfigParser()
    config.read(path)

    config['global']['username'] = '"' + input('4. Enter your spotify username (email):\n') + '"'
    config['global']['password'] = '"' + getpass('5. Enter your spotify password:\n') + '"' 

    with open(path, "+w") as config_file:
        config.write(config_file)

    os.system("systemctl --user start spotifyd.service")
    os.system("systemctl --user enable spotifyd.service")
    os.system("systemctl --user daemon-reload")
    
    print_info("Please make sure that ~/.local/bin is added to your Path variable")
    print_info("This can be added inside of ~/.bashrc with 'export PATH=\"$HOME/.local/bin:$PATH\"'")

def install():
    os.system("./helper.sh")


def print_config_banner():
    print("""  
------------------------------------------------------------
                  ░█▀▀░█▀█░█▀█░█▀▀░▀█▀░█▀▀
                  ░█░░░█░█░█░█░█▀▀░░█░░█░█
                  ░▀▀▀░▀▀▀░▀░▀░▀░░░▀▀▀░▀▀▀
------------------------------------------------------------
""")

def print_install_banner():
    print("""
------------------------------------------------------------
                ░▀█▀░█▀█░█▀▀░▀█▀░█▀█░█░░░█░░
                ░░█░░█░█░▀▀█░░█░░█▀█░█░░░█░░
                ░▀▀▀░▀░▀░▀▀▀░░▀░░▀░▀░▀▀▀░▀▀▀
------------------------------------------------------------
""")

def print_copyfiles_banner():
    print("""
------------------------------------------------------------
           ░█▀▀░█▀█░█▀█░█░█░░░█▀▀░▀█▀░█░░░█▀▀░█▀▀
           ░█░░░█░█░█▀▀░░█░░░░█▀▀░░█░░█░░░█▀▀░▀▀█
           ░▀▀▀░▀▀▀░▀░░░░▀░░░░▀░░░▀▀▀░▀▀▀░▀▀▀░▀▀▀
------------------------------------------------------------
""")

def check_dependencies():
    os.system("sudo clear")
    os.system('[[ "$(python3 -V)" =~ "Python 3" ]] && echo "Python 3 is installed"')
    os.system("clear")

def main(argv):
    try:
        opts, args = getopt.getopt(argv, "hcaif", ["config", "help", "all", "install", "files"])
    except getopt.GetoptError:
        print('install.py -h or install.py --help for help')
        sys.exit(2)
    
    check_dependencies()

    for opt, arg in opts:
        if opt in ('-h', '--help'):
            help()
        if opt in ('-a', '--all'):
            print_install_banner()
            install()
            print_copyfiles_banner()
            files()
            print_config_banner()
            config()
        if opt in ('-c', '--config'):
            print_config_banner()
            config()
        if opt in ('-f', '--files'):
            print_copyfiles_banner()
            files()
        if opt in ('-i', '--install'):
            print_install_banner()
            install()
        

if __name__ == "__main__":
    main(sys.argv[1:])
