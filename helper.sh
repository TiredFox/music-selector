#!/usr/bin/env bash

fail () {
    echo [$'\e[1;31m'FAIL$'\e[0m']"(Trace|$3)" $1
    exit $2
}

success () {
    echo [$'\e[1;32m'SUCCESS$'\e[0m'] $1
}

info () {
    echo [$'\e[1;35m'INFO$'\e[0m'] $1
}

warning () {
    echo [$'\e[1;36m'WARNING$'\e[0m'] $1
}

install_packages () {
     info "Installing pip for pypy"
    if wget https://bootstrap.pypa.io/pip/3.6/get-pip.py -P ~/Downloads/
    then
        if pypy3 ~/Downloads/get-pip.py
        then
            success "Installed pip"
        else
            if [ "$1" == "" ]
            then
                fail "Couldn't install pip" 8 "54"
            else
                fail "Couldn't install pip" 8 "$1:54"
            fi
        fi

        if find ~/Downloads/ -name "get-pip*" -delete
        then
            success "Cleaned up (deleted) pip install files"
        else
            if [ "$1" == "" ]
            then
                fail "Couldn't clean up pip install files" 9 "67"
            else
                fail "Couldn't clean up pip install files" 9 "$1:67"
            fi
        fi
    else
        if [ "$1" == "" ]
        then
            fail "Couldn't download pip install files" 7 "41"
        else
            fail "Couldn't download pip install files" 7 "$1:41"
        fi
    fi
    
    info "Installing pypy package Spotipy"
    if pypy3 -m pip install spotipy
    then
        success "Installed spotipy package"
    else
        if [ "$1" == "" ]
        then
            fail "Couldn't install spotipy package" 10 "80"
        else
            fail "Couldn't install spotipy package" 10 "$1:80"
        fi
    fi
}


if [ "$(grep -Ei 'debian|buntu|mint' /etc/*release)" ]
then
    info "Installing pypy compiler"
    sudo apt install pypy3
    info "Installing spotifyd (spotify daemon)"
    if wget https://github.com/Spotifyd/spotifyd/releases/download/v0.3.3/spotifyd-linux-full.tar.gz -P ~/Downloads/ 2>/dev/null
    then 
        if sudo tar -xvf ~/Downloads/spotifyd-linux-full.tar.gz -C /usr/bin/
        then
            if find ~/Downloads/ -name 'spotifyd-linux-full*' -delete
            then
                success "Installed Spotifyd"
            else
                fail "Couldn't clean up (delete) zipped file at ~/Downloads/*.tar.gz" 12 "251"
            fi
        else
            fail "Couldn't extract spotifyd from tar.gz file in ~/Downloads/" 13 "249"
        fi
    else
        fail "Couldn't download latest version of spotifyd from github" 14 "247"
    fi
    
    #update
    info "Updating"
    sudo apt update
    #install pip and spotipy
    info "Installing Packages"
    install_packages 1 1
else
    info "Installing pypy compiler"
    sudo pacman -Sy pypy3 --noconfirm
    info "Installing spotifyd (spotify daemon)"
    sudo pacman -Sy spotifyd --noconfirm
    #update
    info "Updating"
    sudo pacman -Syy
    #install pip and spotipy
    info "Installing Packages"
    install_packages 1 1
fi