![Current Application Look](/README_images/Spotifli_Current.gif)

## Description

This CLI tool is used to interface with the Spotify API. You can do most things that the normal spotify app does, but instead of using the GUI you can just use the CLI. This application is writting in pure python.

More information found [here](https://docs.google.com/document/d/1O2BIhluqzUyPNReQAssmqeHP7PziyYalyL1STRwQHeM/edit?usp=sharing).

**_Instructions for setup coming soon_**

---

## Installation

0. Make sure to have Python3 and git installed
1. Clone the repository anywhere on your system
2. Navigate into the MusicPlayer folder
3. Run install script `./install.py -a`
5. Use command `spotifli` to use the application
6. Enjoy :)

## Dependencies

- Linux Operating System (Debian or Arch)

- Spotify

  ## All Python dependencies are automatically installed with the install.sh script

  - Python3 - Python language compiler

    ```bash
    sudo apt install python3
    OR
    sudo pacman -S python3
    ```

  - Git - Version and file control software

    ```bash
    sudo apt install git
    OR
    sudo pacman -S git
    ```

## Features:

- Music Player

1. Can play user's saved playlists
2. Can search spotify for tracks, playlists, and artists
3. Can play, pause, shuffle, repeat, and adjust volume
4. Can search and play from user's playlists
5. Displays currently playing track with the artist(s) and the current playback time

## **Usage**

- 1. **Controls**

     | **Command** | **Description**            |
     | :---------: | -------------------------- |
     |   ← , →    | Seek backward/forward |
     | n, p       | Skip forward/backward        |
     | Space       | Toggle Play / Pause |
     | q           | Back / Quite           |
     | - and +         | Decrease  / Increase volume |
     |m       | Mute       |
     | k , l          | Move selection Up/Down |
     | h, k          | Move selection Lef/Right          |
     | u          | Toggle looping |
     | s          | Toggle shuffle         |
     | /          | Search     |

