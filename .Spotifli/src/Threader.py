from threading import Thread
from pathlib import Path
import os

class Thread(Thread):
    
    def run(self):
        try:
            super().run()
        except Exception as e:
            with open(str(Path.home()) + "/.Spotifli/Errors.log", '+a') as Errors:
                Errors.write(e.Message())
