"""UI implementation of the application."""
import curses
import copy
from pathlib import Path


class element:
    """The base object that supports all UI."""

    def __init__(self, loc=[0, 0], content=[], color: int = 4):
        """When constructed it will set the top left corner location along with the content of the object.

        Args:
            loc (list): 2D array that is the coordinates of the top left corner
            content (list): List of objects to be displayed
        """
        self.loc = loc
        if content is not None:
            self.content = content

        self.color = color

    def refresh(self, content=None):
        """Will refresh the object. Will update the content.

        Args:
            content (list, optional): The new content of the object. Defaults to [].

        Returns:
            bool: True if content is changed
        """
        if content is not None:
            self.content = content
            return True
        return False

    def get_content(self):
        """Will return a copy of the objects content.

        Returns:
            list: Copy of content
        """
        return copy.deepcopy(self.content)

    def get_loc(self):
        """Will return the location of the top left corner of the window.

        Returns:
            list: 2D array of the top left corner with format [x,y]
        """
        return copy.deepcopy(self.loc)


class line():
    """The information that will be printed to a line on the display."""

    def __init__(self, content: list, data: list):
        """Object that holds an array of strings representing columns."""
        self.content = content
        self.data = data
    
    def __str__(self):
        out = ''
        for string in self.content:
            out += string + ' '
        return str(out)


class frame(element):
    """A base component for displaying python.curses visual objects.

    Args:
        element (Parent Class): A basic component that has a location of format [x, y] and a content list
    """

    def __init__(self, dim=[10, 10], loc=[0, 0], content=[]):
        """Will call the parent constructor and set the location, content, border, and dimensions to local variables.

        Args:
            dim (list, optional): The dimenstions of the window in the format [width, height]. Defaults to [10,10].
            loc (list, optional): The location of the top left corner in the format [x, y] in relevation to the terminal window. Defaults to [0,0].
            border (bool, optional): Whether the border will be drawn. Defaults to False.
            content (list, optional): The list of pads that will be displayed. Defaults to [].
            title (str, optional): The title of the viewable object. Defaults to "Default".
        """
        self.dim = dim
        super().__init__(loc, content)

    def get_dim(self):
        """Will return the dimenstions of the current viewable object.

        Returns:
            list: 2D array that is the dimensions of the viewable object of format [width, height]
        """
        return copy.deepcopy(self.dim)


class pad(frame):
    """The scrollable window that will display the lines of information."""

    def __init__(self, colors: dict, dim: list=[10, 10], loc: list=[0, 0], content = None, highlight: bool = True):
        """Scrolling visual object based off of curses.pad.

        Args:
            dim (list, optional): Array of the width and height of format [width, height]. Defaults to [10,10].
            loc (list, optional): Array of the x and y location of the object of format [x, y]. Defaults to [0,0].
            content (list, optional): List of content object which will be of type line. Defaults to [].
        """
        super().__init__(dim, loc, content)
        self.colors = colors

        self.pad = curses.newpad(256, 256)
        self.pad.idlok(False)
        self.pad.idcok(False)

        self.cur_row, self.cur_col = 0, 0

        self.highlight = highlight
        self.key = None
        self.content = None

    def refresh(self, content, key: str, row: int, col: int, state: str):
        """Refresh the visual object.

        Args:
            content (list): The list of new content objects
            row (int): The number of rows to move
            col (int): The number of columns to move
        """
        # If content changes reset the current selected row
        if content is not None and self.content != content and self.key is not None and self.key != 'text' and self.key in self.content.parse.keys():
            self.cur_row = len(self.content.parse[self.key]['header'])

        # Replace content if needed
        super().refresh(content)
        
        # Update key for p_search based data
        if key is not None and key != self.key:
            self.key = key

        if self.content is not None and self.key is not None  and self.key != 'text':
            if self.key in self.content.parse.keys():
                if self.cur_row < len(self.content.parse[self.key]['header']):
                    self.cur_row = len(self.content.parse[self.key]['header'])
            
        # Clear the screen
        self.pad.erase()
        
        # Check for valid row and column then assign new values
        if (row != 0):
            new_row = self.cur_row + row
            if (new_row < self.content.parse[self.key]['max_height'] 
            and new_row > len(self.content.parse[self.key]['header']) - 1):
                self.cur_row = new_row
        
        if (col != 0):
            new_col = self.cur_col + col
            if (new_col < self.content.parse[self.key]['max_width'] and new_col >= 0):
                self.cur_col = new_col

            # Add lines
        if self.content is not None and self.key is not None and self.key != 'text' and self.key in self.content.parse.keys():
            lines = self.content.get_keys_lines(self.key)
            for index, child in enumerate(lines):
                if state == 'focus' and self.cur_row == index and self.highlight:
                    self.pad.addstr(index, 1, child, curses.color_pair(self.colors['focus']))
                else:
                    self.pad.addstr(index, 1, child, curses.color_pair(self.colors['text']))
        elif self.key is not None and self.content is not None and self.key == 'text':
            self.pad.addstr(0, 1, self.content, curses.color_pair(self.colors['text']))
        # self.pad.border()

        # curses.pad.refresh(top left corner y visible,      The visible coordinate is the one that
        #                       top left corner x visible,   changes to make scrolling affect
        #                       top left corner y on screen, 
        #                       top left corner x on screen, 
        #                       height, 
    #                       width)
        if self.content is not None and self.key is not None and self.key != 'text' and self.key in self.content.parse.keys():
            scroll = self.cur_row - self.dim[1]+4
            if (scroll < 0) or self.highlight == False:
                scroll = 0
            if scroll > len(lines)-self.dim[1]:
                scroll = len(lines)-self.dim[1]
        else:
            scroll = 0
        self.pad.refresh(scroll, self.cur_col, self.loc[1]+1, self.loc[0]+1,
                         self.loc[1]+self.dim[1], self.loc[0] + self.dim[0])

    def get_data(self, index: int = None):
        #NOTE Make sure to have handling on funtions calling this one
        # It can return null if you are grabbing an index that is header or does not have data
        if index is not None:
            return self.content.get_data(self.key, index)
        else:
            return self.content.get_data(self.key, self.cur_row)

class window(frame):
    """Static visual object that has a title, border, and pad."""

    def __init__(self, colors: dict = None, dim=[10, 10], loc=[0, 0], border=False, title="Default", content=[], state='normal', highlight: bool = True):
        """Viewable object that contains pads.

        Args:
            frame (parent class): The base class that makes it hold the information.
            dim (list, optional): The dimensions of the viewable object. Defaults to [10,10].
            loc (list, optional): The location of the top left corner of the viewable object. Defaults to [0,0].
            border (bool, optional): Whether the border is drawn. Defaults to False.
            title (str): The title of the window
        """
        super().__init__(dim=dim, loc=loc)

        self.colors = colors
        self.border = border
        self.title = title

        self.content = content

        # create the window with the passed in variables
        self.window = curses.newwin(dim[1], dim[0], loc[1], loc[0])
        # Disable special characters to prevent screen flashing
        self.window.idlok(False)
        self.window.idcok(False)

        self.pad = pad(colors, [self.dim[0]-2, self.dim[1]-2], self.loc,
                       [], highlight)

        self.state = state
        # Draw the windows content
        self.window.refresh()

    def refresh(self, state=None, content = None, key: str = None, row: int = 0, col: int = 0, title: str = None, border: bool=True):
        """Will erase the window, then draw the border and the content of the window."""
        # Redraw the windows border and title
        if state is not None and state is not self.state and state != self.state:
            self.state = state
        
        if title is not None and title != self.title:
            self.title = title

        self.window.erase()
        # If window had border add it
        if self.border != border:
            self.border = border
            
        if self.border:
            self.window.attron(curses.color_pair(self.colors[self.state]))
            self.window.border()
            self.window.attroff(curses.color_pair(self.colors[self.state]))


        self.window.attron(curses.color_pair(self.colors['text']))

        self.window.addstr(0, 2, " " + self.title + " ")

        for text in self.content:
            elem = ""
            for string in text.content:
                elem += string
            self.window.addstr(text.loc[1], text.loc[0], elem)
        self.window.attroff(curses.color_pair(self.colors['text']))
        # Draw the window
        self.window.refresh()

        self.pad.refresh(content, key, row, col, self.state)
