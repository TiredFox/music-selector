# !/usr/bin/env pypy3
"""Used to interface with Spotipy which communicates with the spotify API.

Classes:

    API
Misc Variables:

    current_dir
"""
import os
import json
import spotipy
from Threader import Thread
from UI import line
import Data

from pathlib import Path

from spotipy.oauth2 import SpotifyOAuth
current_dir = os.path.dirname(os.path.abspath(__file__))


class API():
    """The main api class that will hold and retreive information form the Spotify API."""

    def __init__(self, config, conf_path):
        """Initialize the API class."""
        self.conf_path = conf_path
        self.config = config
        self.LOGIN = config["Login"]
        self.PLAYER = config["Player"]

        auth = SpotifyOAuth(client_id=str(self.LOGIN['user_id']),
                            client_secret=str(self.LOGIN['user_secret']),
                            redirect_uri="http://localhost:" + str(self.LOGIN['port']) + "/callback",
                            scope=str(self.LOGIN['scope']))
        self.spotify = spotipy.Spotify(auth_manager=auth)
        self.selected_device = None

        self.shuffle = 'off'
        self.repeat = 'off'
        self.cur_vol = 100

        self.check_playback()

        self.PP_Thread = Thread()
        self.Play_Thread = Thread()
        
        # with open(str(Path.home()) + '/Repo/music-selector/Log/SearchResult.log', '+w') as logger:
        #     logger.write(json.dumps(self.spotify.search(q='lofi',type='episode'), indent=2))

    # --------------------- Utility ----------------------

    def save_config(self):
        """Will save the config to the config path given."""
        with open(self.conf_path, "+w") as config_file:
            self.config.write(config_file)

    def convertMillis(self, millis: int):
        """Return formatted string of duration/time.

        Returns:
            str: The duration/time converted from milliseconds
        """
        seconds = (millis//1000) % 60
        minutes = (millis//(1000*60)) % 60
        hours = (millis//(1000*60*60)) % 24

        if hours > 0:
            return str(str(hours) + ':' + str(minutes) + ':' + str(seconds).rjust(2, '0'))
        else:
            return str(str(minutes) + ':' + str(seconds).rjust(2, '0'))

    # --------------------- Player -----------------------
    def play_pause(self):
        """Will toggle the play/pause state of the currently selected device (Multithreaded)."""
        if self.PP_Thread.is_alive() is False:
            if self.playing is False:
                self.PP_Thread = Thread(target=self.spotify.start_playback,
                                        kwargs={"device_id":
                                                self.selected_device})
                self.PP_Thread.start()
                self.playing = True
            else:
                self.PP_Thread = Thread(target=self.spotify.pause_playback,
                                        kwargs={"device_id":
                                                self.selected_device})
                self.PP_Thread.start()
                self.playing = False

    def check_playback(self):
        """Check whether there is something playing for the current user.

        Returns:
            bool: True if something is playing
        """
        self.play_state = self.spotify.current_playback()
        
        if self.play_state is not None:
            self.playing = self.play_state['is_playing']

            # Get volume and mute state
            self.volume = self.play_state['device']['volume_percent']
            if self.volume == 0:
                self.muted = True
            else:
                self.muted = False
            self.cur_vol = self.volume

            # Get shuffle and repeat state
            self.shuffle = self.play_state['shuffle_state']
            self.repeat = self.play_state['repeat_state']

    def play_selected(self, obj_type, position,  uris: list = None, uri: str = None):
        """Play the passed tracks on the currently selected player.

        Args:
            uris (list): The uris of the tracks to be played
        """
        if obj_type == 'tracks':
            self.Play_Thread = Thread(target=self.spotify.start_playback,
                                  kwargs={'uris': uris,
                                          'device_id': self.selected_device,
                                          'offset': {'uri': uris[position]}})
        if obj_type == 'episode':
            self.Play_Thread = Thread(target=self.spotify.start_playback,
                                  kwargs={'context_uri': uri, 'device_id': self.selected_device})
        
        self.Play_Thread.start()
        self.playing = True

    def get_user_playlists(self):
        """Return the list of formatted lines of user's playlists.

        Returns:
            Array: Collection of formatted lines with playlist information
        """
        return Data.p_playlists(self.spotify.current_user_playlists())

    def get_playlist_songs(self, uri):
        """Get the tracks from a playlist

        Args:
            uri (str): The playlist ID

        Returns:
            Data.p_tracks: The formatted data object for the results of the playlist tracks
        """
        return Data.p_playlist_tracks(self.spotify.playlist_tracks(uri.split(':')[-1]))
        
    def get_album_songs(self, uri: str):
        """Get the tracks from an album

        Args:
            uri (str): The album uri

        Returns:
            Data.p_tracks: A formatted data object for the results of the album tracks
        """
        return Data.p_album_tracks(self.spotify.album_tracks(album_id=uri.split(':')[-1]))

    def get_show_episodes(self, uri: str):
        """Get the episodes from a show

        Args:
            uri (str): The show uri

        Returns:
            Data.p_episodes: A formatted data object for the results of the show episodes
        """
        return Data.p_episodes(self.spotify.show_episodes(show_id=uri.split(':')[-1]))

    def get_user_currently_playing(self, width: int):
        """Return the formatted line for the currently playing track.

        Returns:
            str: The formatted playback track with name and duration
        """
        playback = self.spotify.currently_playing()
        if playback is not None:
            return Data.p_currently_playing(playback, width)
        else:
            return None

    def get_search_results(self, search, limit: list = [0, 50],
                           filter: str = 'playlist'):
        """Call the api and returns the line formatted search results.

        Args:
            search (str): Search term that is to be used

        Returns:
            Array: The lines of search results
        """

        return Data.p_search(self.spotify.search(search, limit=50, type='track,album,playlist'))

    # --------------------- Settings  and Utility methods ---------------------

    def devices_get(self):
        """Call api and gets user's devices and returns the results.

        Returns:
            dict: The devices that are available
        """
        return Data.p_devices(self.spotify.devices())
        

    def devices_set(self, device_id: str):
        """Set the currently used device given an index selection.

        Args:
            device_name (str): The name of the selected device
            device_id   (ind): The id of the selected device

        Returns:
            boolean: True if selected device was changed
        """
        self.selected_device = device_id
        self.spotify.transfer_playback(self.selected_device,
                                                       True)
        self.playing = True
        self.check_playback()

    # --------------------------------------------------------------------

    def skip_next(self):
        """Skip to the next song. Multithreaded."""
        if self.PP_Thread.is_alive() is False:
            self.PP_Thread = Thread(target=self.spotify.next_track,
                                        kwargs={"device_id":
                                                self.selected_device})
            self.PP_Thread.start()
            self.playing = True

    def skip_previous(self):
        """Will skip to the previous song. Multithreaded."""
        if self.PP_Thread.is_alive() is False:
            self.PP_Thread = Thread(target=self.spotify.previous_track,
                                        kwargs={"device_id":
                                                self.selected_device})
            self.PP_Thread.start()
            self.playing = True

    def vol_up(self):
        """Will increase the volume by configured amoung. Multithreaded."""
        if self.PP_Thread.is_alive() is False:
            if self.volume + int(self.PLAYER['volume_increment']) > 100:
                self.volume = 100
            else:
                self.volume += int(self.PLAYER['volume_increment'])

            self.PP_Thread = Thread(target=self.spotify.volume,
                                        kwargs={"volume_percent": self.volume, "device_id":
                                                self.selected_device})
            self.cur_vol = self.volume
            self.PP_Thread.start()

    def vol_down(self):
        """Will skip to the next song. Multithreaded."""
        if self.PP_Thread.is_alive() is False:
            if self.volume - int(self.PLAYER['volume_increment']) < 0:
                self.volume = 0
            else:
                self.volume -= int(self.PLAYER['volume_increment'])

            self.PP_Thread = Thread(target=self.spotify.volume,
                                        kwargs={"volume_percent": self.volume, "device_id":
                                                self.selected_device})
            self.cur_vol = self.volume
            self.PP_Thread.start()

    def mute(self):
        """Will toggle the mute. Multithreaded."""
        if self.PP_Thread.is_alive() is False:
            vol = 0
            if self.muted == True:
                vol = self.volume
                self.muted = False
            else:
                self.muted = True

            self.PP_Thread = Thread(target=self.spotify.volume,
                                        kwargs={"volume_percent": vol, "device_id":
                                                self.selected_device})

            self.cur_vol = vol
            self.PP_Thread.start()


    def toggle_repeat(self):
        """Will the repeat for the player. Multithreaded."""
        if self.PP_Thread.is_alive() is False:
            state = 'off'
            if self.repeat == 'off':
                state = 'context'
            elif self.repeat == 'context':
                state = 'track'
            elif self.repeat == 'track':
                state = 'off'

            self.PP_Thread = Thread(target=self.spotify.repeat,
                                    kwargs={"state": state, "device_id":
                                        self.selected_device})
            self.PP_Thread.start()
            self.repeat = state

    def toggle_shuffle(self):
        """Will toggle the shuffle for the player. Multithreaded."""
        if self.PP_Thread.is_alive() is False:
            state = False
            if self.shuffle is False:
                state = True

            self.PP_Thread = Thread(target=self.spotify.shuffle,
                                        kwargs={"state": state, "device_id":
                                                self.selected_device})
            self.PP_Thread.start()
            self.shuffle = state
