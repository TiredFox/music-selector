# 1.0v Final version for release
**New**
1. Arrow keys can be used for navigation
2. Custom threading class that dumps errors to a error log file

**Fixed**
1. Errors not being handles
2. Not being able to launch without a player
3. Other minor bugs

# 0.32v Completed the player with most if not all features
**New**
1. Player will now have split windows for tracks, playlist, and albums when searching
1. Can now choose playlist and ablums from search

**Changed**
1. Parsing structure
1. UI layout
1. currently playing display
1. How the currently playing display updates

**Stretch**
1. Make UI update to the width and height when screen changes size
1. save and view favorite songs
1. Add and remove songs from playlists
1. Save playlist to users account

# 0.31v Added the ability to select a song from the results window
**New**
1. Can select and option from the results window
    * Will now play the song from the selection

**Note**
1. Next version will have the ability to select a playlist option

# 0.3v UI Selection and Player Controls
**New**
1. New controls
    * Volume up
    * Volume Down
    * Play
    * Pause
    * Toggle shuffle mode
    * Toggle repeat mode
    * Next song
    * Previous song
    * Select window
    * Select line in window
    * `d` now brings up device select
1. Player controls can be used anywhere except when typing search result
1. UI
    * Window is highlighted when it is the focused window
    * Line is highlighted when it is the focused window
    * Can focus/select multi purpose window, Playlists window, and Browse window
1. Application will now start in device selection


**Note**
1. No action on selecting a line.
1. Next version will have actions based on selecting a line
1. Need to keep track of what to do if line selected

# 0.2v UI and Searching

**New**
1. Different input modes
1. Can enter search terms
1. Displays currently playing song
1. Displays users playlists
1. Cache data is now saved

**Changed**
1. Full UI Layout
1. Devices are now cached in cache file instead of config

**Fixed**
1. Bad authorization port
2. No Player Bug
    * Now as long as it caches the device once it will work

# 0.1v Config File

**New**
1. Added a config file for the main app
2. Added spotifyd as a daemon / player
3. Added helper python installation script

**Changed**
1. Updated install script to set everything up
2. New way of streaming music (Spotify Daemon)
3. Moved files around to have a better file structure
4. Entire app now uses the config file where possible

